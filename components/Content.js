import React, { Component } from 'react'
import { Text, TextInput, View, TouchableHighlight } from 'react-native'
import styles from '../styles'

export default class Content extends Component {
  constructor(props) {
    super(props)
    this.state = {
      containerMass: 0,
      sampleMass: 0,
      sampleVolume: 0,
    }
  }

  componentDidUpdate() {
    this.calculateDensity()
  }

  calculateDensity() {
    const density = this.state.sampleMass / this.state.sampleVolume
    return (density > 0 && density != 'Infinity') ? density : 0
  }

  render() {
    return (
      <View style={styles.content}>
        <View style={styles.fieldWrapper}>
          <View style={styles.labelWrapper}>
            <Text style={styles.labelText}>
              CONTAINER MASS
            </Text>
          </View>
          <TextInput 
            style={styles.inputText}
            value={this.state.containerMass + ''}
            onChangeText={value => this.setState({ containerMass: value })}
          />
          <Text style={styles.unitText}>g</Text>
        </View>
        <View style={styles.fieldWrapper}>
          <View style={styles.labelWrapper}>
            <Text style={styles.labelText}>
              SAMPLE MASS
            </Text>
          </View>
          <TextInput 
            style={styles.inputText}
            value={this.state.sampleMass + ''}
            onChangeText={value => this.setState({ sampleMass: value })}
          />
          <Text style={styles.unitText}>g</Text>
        </View>
        <View style={styles.fieldWrapper}>
          <View style={styles.labelWrapper}>
            <Text style={styles.labelText}>
              SAMPLE VOLUME
            </Text>
          </View>
          <TextInput 
            style={styles.inputText}
            value={this.state.sampleVolume + ''}
            onChangeText={value => this.setState({ sampleVolume: value })}
          />
          <Text style={styles.unitText}>mL</Text>
        </View>
        <View style={styles.fieldWrapper}>
          <View style={styles.labelWrapper}>
            <Text style={styles.labelText}>
              SAMPLE DENSITY
            </Text>
          </View>
          <TextInput 
            style={styles.inputText}
            value={this.calculateDensity() + ''}
            editable={false}
          />
          <Text style={styles.unitText}>g/mL</Text>
        </View>
        <View style={styles.fieldWrapper}>
          <View style={styles.labelWrapper}>
            <Text style={styles.labelText}>
              SPECIFIC GRAVITY
            </Text>
          </View>
          <TextInput 
            style={styles.inputText}
            value={this.calculateDensity() + ''}
            editable={false}
          />
        </View>
      </View>
    )
  }
}