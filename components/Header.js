import React, { Component } from 'react'
import { Text, View } from 'react-native'
import styles from '../styles'

export default class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.titleText}>SPECIFIC GRAVITY</Text>
        <Text style={styles.titleText}>CALCULATOR</Text>
        <Text style={styles.smallText}>BY JOEL TUMAMBO</Text>
      </View>
    )
  }
}