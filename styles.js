import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    width: '100%',
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40
  },
  header: {
    height: '20%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    color: '#fff',
    fontSize: 26,
    fontFamily: 'VCR-OCD-Mono'
  },
  smallText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'VCR-OCD-Mono'
  },
  fieldWrapper: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 2,
    width: '100%',
    borderColor: 'grey',
    marginBottom: 20
  },
  labelWrapper: {
    backgroundColor: 'grey',
    padding: 20
  },
  inputWrapper: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
  },
  labelText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'VCR-OCD-Mono'
  },
  inputText: {
    marginLeft: 20,
    color: '#fff',
    fontSize: 20,
    fontFamily: 'VCR-OCD-Mono'
  },
  unitText: {
    paddingTop: 20,
    color: '#fff',
    fontSize: 20,
    fontFamily: 'VCR-OCD-Mono'
  }
})

export default styles
