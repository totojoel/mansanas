import React, { Component } from 'react'
import { View } from 'react-native'
import { Font } from 'expo'
import styles from './styles'

import Header from './components/Header'
import Content from './components/Content'

export default class App extends Component {
  async componentDidMount() {
    await Font.loadAsync({
      'VCR-OCD-Mono': require('./assets/vcr.ttf')
    })
    this.setState({ fontLoaded: true })
  }

  constructor(props) {
    super(props)
    this.state = {
      fontLoaded: false,
    }
  }
  render() {
    return (
      this.state.fontLoaded ?
      <View style={styles.container}>
        <Header />
        <Content />
      </View> : null
    )
  }
}
